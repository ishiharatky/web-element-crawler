import os
import time
from multiprocessing import Pool
from lxml import etree as et
from typing import Optional, Tuple

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


SAVE_IMG_DIR = "img"
SAVE_XML_DIR = "xml"
CRAWLING_TARGET_URLS = "URLLIST.txt"
TARGET_TAGS = ["tbody", "th", "td"]
NUM_OF_PROCESS = 16

def save_xml(fname: str, object_locations: list, page_width: int, page_height: int):
    """
        object_locationsの情報を基にxmlファイルを生成する
    """
    # objectを記述する以外の箇所を作成する
    root = et.Element('annotation')
    tree = et.ElementTree(element=root)
    folder = et.SubElement(root, 'folder')
    folder.text = SAVE_XML_DIR
    filename = et.SubElement(root, 'filename')
    filename.text = fname.replace('.xml', '.png')
    path = et.SubElement(root, 'path')
    path.text = os.path.join(os.getcwd(), fname.replace('.xml', '.png'))
    source = et.SubElement(root, 'source')
    database = et.SubElement(source, 'database')
    database.text = 'Unknown'
    size = et.SubElement(root, 'size')
    width = et.SubElement(size, 'width')
    height = et.SubElement(size, 'height')
    depth = et.SubElement(size, 'depth')
    width.text = str(page_width)
    height.text = str(page_height)
    depth.text = '3'
    segmented = et.SubElement(root, 'segmented')
    segmented.text = '0'
    # オブジェクトを1つずつ追加する
    for object_location in object_locations:
        object = et.SubElement(root, 'object')
        name = et.SubElement(object, 'name')
        pose = et.SubElement(object, 'pose')
        truncated = et.SubElement(object, 'truncated')
        difficult = et.SubElement(object, 'difficult')
        name.text = object_location["tag_name"]
        pose.text = 'Unspecified'
        truncated.text = '0'
        difficult.text = '0'
        bndbox = et.SubElement(object, 'bndbox')
        xmin = et.SubElement(bndbox, 'xmin')
        ymin = et.SubElement(bndbox, 'ymin')
        xmax = et.SubElement(bndbox, 'xmax')
        ymax = et.SubElement(bndbox, 'ymax')
        xmin.text = str(object_location["x"])
        ymin.text = str(object_location["y"])
        xmax.text = str(object_location["x"] + object_location["width"])
        ymax.text = str(object_location["y"] + object_location["height"])
    
    tree.write(os.path.join(SAVE_XML_DIR, fname), encoding='utf-8', pretty_print=True)


def launch_selenium(url: str) -> Optional[webdriver.remote.webdriver.WebDriver]:
    """
        seleniumを立ち上げたのちに引数で受け取ったurlに遷移する
        
        return:
            driver: webdriver.remote.webdriver.WebDriver
    """
    options = Options()
    options.add_argument('--headless')
    options.add_argument("start-maximized")
    options.add_argument("enable-automation")
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-infobars")
    options.add_argument('--disable-extensions')
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--disable-browser-side-navigation")
    options.add_argument("--disable-gpu")
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--ignore-ssl-errors')
    prefs = {"profile.default_content_setting_values.notifications" : 2}
    options.add_experimental_option("prefs",prefs)
    try:
        driver = webdriver.Remote(
            command_executor='http://localhost:4444/wd/hub',
            desired_capabilities=options.to_capabilities(),
            options=options
        )
        driver.get(url)
        return driver 
    except:
        return None


def take_screenshot(fname: str, driver: webdriver.remote.webdriver.WebDriver) -> Tuple[bool, int, int]:
    """
        Webページ全体のスクリーンショットを撮る
        ※headlessモードで動いている前提

        return:
            is_success: boolean
    """
    try:
        page_width = driver.execute_script('return document.body.scrollWidth')
        page_height = driver.execute_script('return document.body.scrollHeight')
        # ページ全体の高さと幅を取得してwindow sizeとして設定する
        # headlessモードにしていない場合はこの値がモニターの解像度に依存するため注意
        driver.set_window_size(page_width, page_height)
        time.sleep(3)
        return driver.save_screenshot(os.path.join(SAVE_IMG_DIR, fname)), page_width, page_height
    except:
        return False, 0, 0


def extract_object_locations(driver: webdriver.remote.webdriver.WebDriver) -> list:
    """
        ターゲットとなるタグをページから発見し、そのlocationを取得する

        return:
            object_locations: list
    """
    object_locations = []
    for target_tag in TARGET_TAGS:
        try:
            elems = driver.find_elements_by_tag_name(target_tag)
        except:
            return []
        for elem in elems:
            elem_info = {}
            elem_info["tag_name"] = target_tag
            elem_info["x"] = int(elem.location["x"])
            elem_info["y"] = int(elem.location["y"])
            elem_info["width"] = int(elem.size["width"])
            elem_info["height"] = int(elem.size["height"])
            object_locations.append(elem_info)
    return object_locations


def close_webdriver(driver: webdriver.remote.webdriver.WebDriver):
    driver.close()
    driver.quit()


def crawler(i: int, url: str) -> bool:
    # すでにクローリング済みの場合飛ばす
    if os.path.exists(os.path.join(SAVE_XML_DIR, str(i).zfill(8) + '.xml')):
        print(i, ": already scraped")
        return
    print("--------------------", i, url, "--------------------")
    
    driver = launch_selenium(url)
    if driver is None:
        print("driverの立ち上げに失敗しました")
        return
    
    is_take_screenshot_sccess, page_width, page_height = take_screenshot(str(i).zfill(8) + '.png', driver)
    if is_take_screenshot_sccess == False:
        print("スクリーンショットの撮影に失敗しました")
        close_webdriver(driver)
        return
    
    object_locations = extract_object_locations(driver)
    if len(object_locations) == 0:
        print("ターゲットタグが1つも含まれていないようです")
        close_webdriver(driver)
        return
    
    save_xml(str(i).zfill(8) + '.xml', object_locations, page_width, page_height)
    close_webdriver(driver)


def create_crawler_args(url_list: list) -> tuple:
    """
        並列処理のために引数をタプルでまとめる
        なおxmlフォルダを確認してすでにクロール済みのURLは除外する
    """
    new_url_list = []
    for i in range(len(url_list)):
        if os.path.exists(os.path.join(SAVE_XML_DIR, str(i).zfill(8) + '.xml')) == False:
            new_url_list.append((i, url_list[i]))
    return tuple(new_url_list)


def main():
    if not os.path.exists(SAVE_IMG_DIR):
        os.mkdir(SAVE_IMG_DIR)
    if not os.path.exists(SAVE_XML_DIR):
        os.mkdir(SAVE_XML_DIR)
    
    with open(CRAWLING_TARGET_URLS) as f:
        # remove \n
        url_list = [s.strip() for s in f.readlines()]
    
    crawler_args = create_crawler_args(url_list)

    with Pool(NUM_OF_PROCESS) as pool:
        pool.starmap(crawler, crawler_args)


if __name__ == "__main__":
    main()
