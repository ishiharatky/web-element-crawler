# WEB-ELEMENT-CRAWLER
URLのリストを与えると以下のことを行います。
  
- 各ページでターゲットとして指定されたタグを探索し、その座標情報を記録する(xmlファイルの作成)
- 各ページ全体のスクリーンショットを保存する(画像ファイルの作成)

大郷さんのクローラを基に作成しました。感謝です。

# Requirement
seleniumが動く環境(dockerで[standalone-chrome](https://hub.docker.com/r/selenium/standalone-chrome/)を導入すると非常に楽)

# Usage
1. crawer.pyを開き、以下のパラメータをセットする
	- SAVE_IMG_DIR: スクリーンショットを保存するディレクトリ  
	- SAVE_XML_DIR: タグの座標情報を記録したxmlファイルを保存するディレクトリ  
	- CRAWLING_TARGET_URLS: クローリング対象のURLが改行区切りで記述されているテキストファイル  
	- TARGET_TAGS: ターゲットとするタグ。リストで指定。  
	- NUM_OF_PROCESS: 本プログラムで使用するコア数。intで指定。  

2. standalone-chromeの実行
	```
	sudo docker run -d -v /dev/shm:/dev/shm -p 4444:4444 --name chrome_no_option selenium/standalone-chrome
	```
    
3. crawlerの実行
	```
	python3 crawler.py
	```

# Note
seleniumの不具合なのかメモリ不足なのか不明ですが異常終了することがあります。
その場合もう一度手順3を実行してください（すでにクローリング済みのURLは飛ばすようになっています）
